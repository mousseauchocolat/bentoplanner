package org.ict.pck.bentoplanner.listener;

import android.view.MenuItem;
import android.widget.PopupMenu;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.adapter.BentoMenuAdapter;
import org.ict.pck.bentoplanner.menu.DailyMenu;
import org.ict.pck.bentoplanner.menu.IngredientSelector;

/**
 * Created by mi141320 on 2015/09/16.
 */
public class PopupMenuClickListener implements PopupMenu.OnMenuItemClickListener{
	private DailyMenu.RepaintCallback callback;
	private BentoMenuAdapter bentoMenuAdapter;
	private DailyMenu bento;
	private int itemPosition;

	public PopupMenuClickListener(DailyMenu.RepaintCallback callback, BentoMenuAdapter adapter, DailyMenu bento, int position) {
		this.callback = callback;
		this.bento = bento;
		this.bentoMenuAdapter = adapter;
		this.itemPosition = position;
	}
	@Override
	public boolean onMenuItemClick(MenuItem menuItem) {

		switch (menuItem.getItemId()){
			case R.id.popup_menu_item1:
				this.bento.changeToLeftover(itemPosition, this.callback);
				this.bentoMenuAdapter.notifyDataSetChanged();
				return true;
			case R.id.popup_menu_item2:
				IngredientSelector.getInstance().addHateIngredient(this.bento.getIngredients().get(itemPosition));
				this.bento.change(itemPosition, this.callback);
				this.bentoMenuAdapter.notifyDataSetChanged();
				return true;
			case R.id.popup_menu_item3:
				this.bento.change(itemPosition, this.callback);
				this.bentoMenuAdapter.notifyDataSetChanged();
				return true;
			case R.id.popup_menu_item4:
				return true;
		}
		return false;
	}
}
