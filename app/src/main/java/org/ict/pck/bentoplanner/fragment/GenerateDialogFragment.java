package org.ict.pck.bentoplanner.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.utils.PreferenceUtil;

/**
 * Created by mi141320 on 2015/11/02.
 */
public class GenerateDialogFragment extends DialogFragment {
    private ImageButton[] tako = new ImageButton[5];
    private int countPerson;

    public interface OnDialogListener {
        public void onDialogClick(int count);
    }

    private OnDialogListener mListener;

    public static GenerateDialogFragment newInstance(Bundle bundle, Fragment fragment){
        GenerateDialogFragment instance = new GenerateDialogFragment();
        instance.setArguments(bundle);
        instance.setTargetFragment(fragment, 0);
        return instance;
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        Fragment target = this.getTargetFragment();

        mListener = (target != null) ? (OnDialogListener)target : (OnDialogListener)activity;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

        dialog.setContentView(R.layout.dialog_generate);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.findViewById(R.id.fix_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (countPerson != 0){
                    mListener.onDialogClick(countPerson);
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "人数を選択して下さい。", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.findViewById(R.id.cancel_fix_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        final int[] takoId = {
                R.id.tako1, R.id.tako2, R.id.tako3, R.id.tako4, R.id.tako5
        };

        for (int i = 0; i < 5; i++) {
            tako[i] = (ImageButton) dialog.findViewById(takoId[i]);
            tako[i].setImageResource(R.drawable.tako);
        }

        countPerson = 0;

        for (int i = 0; i < 5; i++) {
            final int pos = i;
            tako[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int j = 0; j < 5; j++) {
                        if (j <= pos) {
                            Log.v("count", "" + pos);
                            tako[j].setImageResource(R.drawable.tako_p);
                        } else {
                            tako[j].setImageResource(R.drawable.tako);
                            Log.v("switch", j + "");
                        }
                    }
                    countPerson = pos + 1;
                }
            });
        }

        return dialog;
    }
}
