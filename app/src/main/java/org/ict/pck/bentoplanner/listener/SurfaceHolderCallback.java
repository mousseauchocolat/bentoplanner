package org.ict.pck.bentoplanner.listener;

import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;

/**
 * Created by ic131240 on 2015/08/28.
 */
public class SurfaceHolderCallback implements SurfaceHolder.Callback {

	Camera camera;

	public SurfaceHolderCallback(Camera camera){
		this.camera = camera;
	}

	public SurfaceHolderCallback(){
	}

	@Override
	public void surfaceCreated(SurfaceHolder surfaceHolder) {
		this.camera = Camera.open();
		try {
			this.camera.setPreviewDisplay(surfaceHolder);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
		/*
		this.camera.stopPreview();
		Camera.Parameters parameters = this.camera.getParameters();
		Camera.Size size = parameters.getSupportedPreviewSizes().get(0);
		parameters.setPreviewSize(size.width, size.height);
		this.camera.setParameters(parameters);
		*/
		this.camera.startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
		this.camera.stopPreview();
		this.camera.release();
		this.camera = null;
		Log.v("Photo", "Release");
	}

	public Camera getCamera(){
		return this.camera;
	}
}
