package org.ict.pck.bentoplanner.img;

/**
 * Created by ringoh72 on 2015/10/03.
 */
public class ColorConverter {
	public static ColorName valueOf(String name){
		if (name.equals("赤")){
			return ColorName.RED;
		}
		else if (name.equals("緑")){
			return ColorName.GREEN;
		}
		else if (name.equals("黄色")){
			return ColorName.YELLOW;
		}
		else {
			return ColorName.OTHER;
		}
	}

	public static double[] rgbToArray(int rgb){
		double[] values = new double[4];
		values[0] = rgb / 0x10000;
		values[1] = rgb / 0x100 % 0x100;
		values[2] = rgb % 0x100;
		values[3] = 255;

		return values;
	}
}
