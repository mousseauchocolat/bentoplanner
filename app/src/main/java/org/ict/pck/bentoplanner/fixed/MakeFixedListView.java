package org.ict.pck.bentoplanner.fixed;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by mi141320 on 2015/09/10.
 */
public class MakeFixedListView {
    public static void makeFixedListView(ListView listView, int offset) {
        int totalHeight = offset;
        ListAdapter adapter = listView.getAdapter();
        int headerCount = listView.getHeaderViewsCount();
        int footerCount = listView.getFooterViewsCount();
        int itemCount = adapter.getCount();

        for (int i = 0; i < itemCount; i++) {
            View listItem = adapter.getView(i, null, listView);
            if (i < headerCount || i >= (itemCount - footerCount)) {
                listItem.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,
                        AbsListView.LayoutParams.WRAP_CONTENT));
            } else if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
