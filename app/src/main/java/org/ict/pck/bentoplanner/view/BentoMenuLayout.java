package org.ict.pck.bentoplanner.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.menu.Ingredient;

/**
 * Created by mi141320 on 2015/10/18.
 */
public class BentoMenuLayout extends LinearLayout {
    TextView numberText;
    TextView menuText;
    ImageButton favButton;
    protected boolean favFlag;

    public BentoMenuLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        numberText = (TextView)findViewById(R.id.number_text);
        menuText = (TextView)findViewById(R.id.menu_text);
        favButton = (ImageButton)findViewById(R.id.fav_button);

    }

    public void bindView(Ingredient ingredient, int position){
        numberText.setText(String.valueOf(position + 1));
        menuText.setText(ingredient.getName());
        if (ingredient.isLoved()){
            favButton.setImageResource(R.drawable.star_p);
            this.favFlag = true;
        }
    }
}