package org.ict.pck.bentoplanner.menu;

/**
 * Created by ic131240 on 2015/09/09.
 */
public enum Category {
	MAIN,
	SUB,
	DESSERT,
	LEFTOVER;

	public static Category valueOf(int num){
		if (num == 0){
			return MAIN;
		}
		else if (num != 4){
			return SUB;
		}
		else {
			return DESSERT;
		}
	}
}
