package org.ict.pck.bentoplanner.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.listener.CameraPictureCallback;
import org.ict.pck.bentoplanner.listener.SurfaceHolderCallback;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentBeforeListener} interface
 * to handle interaction events.
 * Use the {@link TakeBeforeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TakeBeforeFragment extends Fragment {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

	// TODO: Rename and change types of parameters

	private OnFragmentBeforeListener listener;
	private Activity activity;
	private SurfaceView surfaceView;
	private SurfaceHolderCallback surfaceHolderCallback;
	private CameraPictureCallback cameraPictureCallback;
	private ImageButton goBackHomeButton;
	private ImageButton photoButton;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @return A new instance of fragment TakeBeforeFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static TakeBeforeFragment newInstance(Activity activity) {
		TakeBeforeFragment fragment = new TakeBeforeFragment();
		fragment.setActivity(activity);
		return fragment;
	}

	public TakeBeforeFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_take_before, container, false);

		this.goBackHomeButton = (ImageButton)rootView.findViewById(R.id.go_back_home_button);
		this.goBackHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onButtonPressed(0);
			}
		});

		this.surfaceView = (SurfaceView)rootView.findViewById(R.id.camera_view);
		this.surfaceHolderCallback = new SurfaceHolderCallback();
		this.surfaceView.getHolder().addCallback(this.surfaceHolderCallback);
		this.cameraPictureCallback = new CameraPictureCallback(this, this.activity);

		this.photoButton = (ImageButton)rootView.findViewById(R.id.take_button);
		this.photoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				TakeBeforeFragment.this.photoButton.setOnClickListener(null);
				//写真を保存
				TakeBeforeFragment.this.surfaceHolderCallback
						.getCamera().takePicture(new Camera.ShutterCallback() {
							@Override
							public void onShutter() {
							}
						}, null, TakeBeforeFragment.this.cameraPictureCallback);
				Log.v("Photo", "take");
				//onButtonPressed(1);
			}
		});

		return rootView;
	}

	@Override
	public void onStart(){
		super.onStart();
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(int eventType) {
		listener.onFragmentBefore(eventType);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (activity instanceof OnFragmentBeforeListener){
			this.listener = (OnFragmentBeforeListener) activity;
		}
		this.activity = activity;

	}

	/*
	@Override
	public void onPause(){
		super.onPause();
		//this.surfaceView.getHolder().removeCallback(this.surfaceHolderCallback);
		this.surfaceHolderCallback.getCamera().stopPreview();
		this.surfaceHolderCallback.getCamera().release();
	}

	@Override
	public void onResume(){
		super.onResume();
		this.surfaceHolderCallback = new SurfaceHolderCallback();
		this.surfaceView.getHolder().addCallback(this.surfaceHolderCallback);
	}
	*/

	@Override
	public void onDetach() {
		super.onDetach();
		listener = null;
		activity.finish();
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentBeforeListener {
		public void onFragmentBefore(int eventType);
	}

	public void setActivity(Activity activity){
		this.activity = activity;
	}
}
