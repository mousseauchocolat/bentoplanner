package org.ict.pck.bentoplanner.img;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.Pair;

import org.ict.pck.bentoplanner.menu.Ingredient;
import org.ict.pck.bentoplanner.packing.PackingPattern;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * 画像処理用クラス
 * インスタンスの生成は考えていません
 * @author ringoh72
 *
 */
public class ImgprocEngine {

	/**
	 * ビットマップから行列に変換
	 * @param bitmap ビットマップ
	 * @return 変換後Matデータ
	 */
	public static Mat bitmapToMat(Bitmap bitmap){
		Mat tmp = new Mat();
		Utils.bitmapToMat(bitmap, tmp);
		return tmp;
	}

	/**
	 * MatのデータをBitmapに変換
	 * @param mat Mat型の画像データ
	 * @return Bitmap型の画像データ
	 */
	public static Bitmap matToBitmap(Mat mat){
		Bitmap tmp = Bitmap.createBitmap(mat.width(), mat.height(), Bitmap.Config.ARGB_8888);
		Utils.matToBitmap(mat, tmp);
		return tmp;
	}

	/**
	 * RGBからHSVに変換
	 * @param mat 変換元画像データ
	 * @return HSV変換後画像データ
	 */
	public static Mat rgbToHSV(Mat mat){
		Mat tmp = new Mat();
		Imgproc.cvtColor(mat, tmp, Imgproc.COLOR_RGB2HSV);
		return tmp;
	}

	/**
	 * HSVからRGBに変換
	 * @param mat 変換元画像データ
	 * @return RGB変換後画像データ
	 */
	public static Mat hsvToRGB(Mat mat){
		Mat tmp = new Mat();
		Imgproc.cvtColor(mat, tmp, Imgproc.COLOR_HSV2RGB);
		return tmp;
	}

	/**
	 * 前景抽出(背景除去)
	 * @param mat 変換元画像データ
	 * @return 前景抽出後画像データ
	 */
	public static Mat foreground(Mat mat){
		Rect rectangle = new Rect(25, 25, mat.cols() - 64, mat.rows() - 64);
		Mat result = new Mat();
		Mat bgdModel = new Mat();
		Mat fgdModel = new Mat();
		Mat src = new Mat(1, 1, CvType.CV_8U, new Scalar(3));

		Imgproc.grabCut(mat, result, rectangle, bgdModel, fgdModel, 1, 0);
		Core.compare(result, src, result, Core.CMP_EQ);
		Mat tmp = new Mat(mat.size(), CvType.CV_8UC1, new Scalar(0, 0, 0));
		mat.copyTo(tmp, result);

		return tmp;
	}

	/**
	 * 特定色抽出
	 * @param mat 変換元画像データ
	 * @param code 抽出する色
	 * @return 抽出画像データ
	 */
	public static Mat detect(Mat mat, ColorName code){
		Scalar lower, upper;
		switch (code){
			case RED:
				lower = new Scalar(140, 80, 80);
				upper = new Scalar(190, 255, 255);
				break;
			case GREEN:
				lower = new Scalar(40, 50, 50);
				upper = new Scalar(90, 255, 255);
				break;
			case YELLOW:
				lower = new Scalar(0, 120, 120);
				upper = new Scalar(40, 255, 255);
				break;
			default:
				lower = new Scalar(0, 0, 0);
				upper = new Scalar(255, 255, 255);
				break;
		}

		Mat tmp = new Mat();
		Core.inRange(mat, lower, upper, tmp);
		return tmp;
	}

	/**
	 * グレースケール化
	 * @param mat 変換元画像データ
	 * @return グレースケール画像データ
	 */
	public static Mat rgbToGray(Mat mat){
		Mat tmp = new Mat();

		Imgproc.cvtColor(mat, tmp, Imgproc.COLOR_RGB2GRAY);
		return tmp;
	}

	/**
	 * リサイズ用
	 * @param mat 変換元画像データ
	 * @param f 倍率
	 * @return リサイズ後画像データ
	 */
	public static Mat resize(Mat mat, double f){
		Mat tmp = new Mat();
		double cols = mat.cols() * f;
		double rows = mat.rows() * f;

		Imgproc.resize(mat, tmp, new Size(cols, rows));
		return tmp;
	}

	/**
	 * しきい値以上の色面積(RGB全部がしきい値以上じゃないとカウントしない)
	 * @param mat 画像データ
	 * @param minR 赤のしきい値
	 * @param minG 緑のしきい値
	 * @param minB 青のしきい値
	 * @return 色面積
	 */
	public static int surface(Mat mat, int minR, int minG, int minB){
		int s = 0;

		/*
		List<MatOfPoint> contour = new ArrayList<MatOfPoint>();
		Imgproc.findContours(mat, contour, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CV_CHAIN_CODE);
		*/
		for (int i = 0; i < mat.height(); i++){
			for (int j = 0; j < mat.width(); j++){
				if (0 < mat.get(i, j)[0]){
					s++;
				}
			}
		}

		return s;
	}

	/**
	 * 白(ffffff)の面積
	 * @param mat 画像データ
	 * @return 白の面積
	 */
	public static int surfaceWhite(Mat mat){
		return surface(mat, 255, 255, 255);
	}

	/**
	 * 弁当の採点
	 * @param mat 弁当の画像データ
	 * @return 点数(0から5まで6段階と各色の割合)
	 */
	public static List<Integer> gradeBento(Mat mat){
		double r = surfaceWhite(ImgprocEngine.detect(mat, ColorName.RED));
		double y = surfaceWhite(ImgprocEngine.detect(mat, ColorName.YELLOW));
		double g = surfaceWhite(ImgprocEngine.detect(mat, ColorName.GREEN));
		double min = mat.cols() * mat.rows() / 100;
		List<Integer> result = new ArrayList<Integer>();

		Log.d("Grading", "min: " + min);
		Log.d("Grading", "red: " + r);
		Log.d("Grading", "yellow: " + y);
		Log.d("Grading", "green: " + g);

		if (r < min){
			r = 0;
		}
		if (y < min){
			y = 0;
		}
		if (g < min){
			g = 0;
		}

		if (r == 0){
			if (y == 0){
				result.add(0);
				result.add(0);
				if (g == 0){
					result.add(0);
					result.add(0);
				}
				else {
					result.add(1);
					result.add(2);
				}
				return result;
			}
			g /= y;
			y = 1;
			result.add(0);
			result.add(1);
			result.add((int)g);
			if (y != 0 && g != 0){
				result.add(3);
			}
			result.add(g == 0 ? 1 : 2);
		}
		else {
			y /= r;
			g /= r;
			r = 1;

			result.add(1);
			result.add((int)y);
			result.add((int)g);
			if (0.8 < y && y < 1.2 && 1.3 < g && g < 1.7) {
				result.add(5);
			} else if (y != 0 && g != 0) {
				result.add(4);
			} else if (y != 0 || g != 0) {
				result.add(3);
			} else {
				result.add(1);
			}
		}

		return result;
	}

	/*
	public static void fill(Bitmap bitmap, Pair<Integer, Integer> point, ColorName color){
		int stopColor = 0x4d2200ff;
		int rgb = ColorConverter.getRGB(color) | 0xff000000;

		Deque<Pair<Integer, Integer>> deque = new LinkedBlockingDeque<Pair<Integer, Integer>>();
		deque.push(point);
		while (!deque.isEmpty()){
			point = deque.pop();
			int y = point.first;
			int x = point.second;
			int pixel = bitmap.getPixel(x, y);

			if (pixel == stopColor || pixel == rgb){
				continue;
			}
			bitmap.setPixel(x, y, rgb);
			deque.push(new Pair<Integer, Integer>(y - 1, x));
			deque.push(new Pair<Integer, Integer>(y + 1, x));
			deque.push(new Pair<Integer, Integer>(y, x - 1));
			deque.push(new Pair<Integer, Integer>(y, x + 1));
		}
	}
	*/

	public static Bitmap fill(PackingPattern packingPattern, List<Ingredient> ingredientList){
		Bitmap bitmap = Bitmap.createBitmap(packingPattern.getBitmap());
		Mat mat = ImgprocEngine.bitmapToMat(bitmap);
		Mat hsv = ImgprocEngine.rgbToHSV(mat);

		/*
		Mat t1 = new Mat();
		Core.inRange(hsv, new Scalar(0, 0, 0), new Scalar(255, 100, 220), t1);
		Mat t2 = new Mat();
		Core.inRange(hsv, new Scalar(0, 0, 254), new Scalar(0, 0, 254), t2);
		*/

		Mat mask = new Mat();
		Core.inRange(hsv, new Scalar(0, 0, 254), new Scalar(0, 0, 254), mask);
		//Core.bitwise_or(t1, t2, mask);
		Mat filled = mat.clone();
		List<Pair<Integer, Integer>> pointList = packingPattern.getPoints();

		for (int i = 0; i < pointList.size(); i += 2){
			Pair<Integer, Integer> lt = pointList.get(i);
			Pair<Integer, Integer> rb = pointList.get(i + 1);
			double[] colorCode = ingredientList.get(i / 2).getColorCode();

			Point p1 = new Point(lt.second, lt.first);
			Point p2 = new Point(rb.second, rb.first);
			Core.rectangle(filled, p1, p2, new Scalar(colorCode), -1);
		}
		//mat.copyTo(filled, white);
		//bitmap = ImgprocEngine.matToBitmap(filled);
		filled.copyTo(mat, mask);
		bitmap = ImgprocEngine.matToBitmap(mat);

		return bitmap;
	}
}
