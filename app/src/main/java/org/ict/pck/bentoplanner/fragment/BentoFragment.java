package org.ict.pck.bentoplanner.fragment;

import android.app.Activity;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.adapter.BentoAdapter;
import org.ict.pck.bentoplanner.menu.AsyncMakeMenu;
import org.ict.pck.bentoplanner.menu.DailyMenu;
import org.ict.pck.bentoplanner.utils.PreferenceUtil;
import org.ict.pck.bentoplanner.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnMenuGenerateListener} interface
 * to handle interaction events.
 * Use the {@link BentoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BentoFragment extends Fragment implements GenerateDialogFragment.OnDialogListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters
    private ListView mainListView;
    private BentoAdapter bentoAdapter;
    private List<DailyMenu> bentoPlan;
    private LinearLayout listViewHeader;
    private final String filename =  "count_save";
    private final String countKey = "count_key";

    private static final String POSITION_KEY = "position_key";
    private int position;
    private OnMenuGenerateListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     */
    // TODO: Rename and change types and number of parameters
    public static BentoFragment newInstance(int pos) {
        BentoFragment fragment = new BentoFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION_KEY, pos);
        fragment.setArguments(args);
        Log.v("BentoFragment", "" + pos);
        return fragment;
    }

    public BentoFragment() {
        Bundle args = new Bundle();
        this.setArguments(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(POSITION_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //((ActionBarActivity)getActivity()).getSupportActionBar().setTitle(Utils.getWeekTitle(position + 1));
        Log.v("pos", position + "");
        View rootView = inflater.inflate(R.layout.fragment_bento, container, false);

        mainListView = (ListView)rootView.findViewById(R.id.main_list_view);
        listViewHeader = (LinearLayout)inflater.inflate(R.layout.bento_plan_header_layout, mainListView, false);
        final Button generateButton = (Button)listViewHeader.findViewById(R.id.make_new_menu_button);
        /*SharedPreferences preferences = getActivity().getSharedPreferences(filename, Context.MODE_PRIVATE);
        String count = preferences.getString(countKey, "114514");*/

        /*if (!count.equals("114514")) {
        } else {

        }*/

        generateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogFragment dialogFragment = GenerateDialogFragment.newInstance(new Bundle(), BentoFragment.this);
                dialogFragment.show(getFragmentManager(), "hoge");

                onButtonPressed();
            }
        });

        if (bentoPlan.size() == 0) {
            mainListView.addHeaderView(listViewHeader);
        }

        mainListView.setAdapter(bentoAdapter);

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onMenuGenerate();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bentoPlan = new ArrayList<DailyMenu>();
        bentoAdapter = new BentoAdapter(activity, R.layout.bento_layout, bentoPlan);
        mListener = (OnMenuGenerateListener)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().finish();
        mListener = null;
    }

    @Override
    public void onDialogClick(int count) {
        Toast.makeText(this.getActivity(), "メニュー作成中...", Toast.LENGTH_SHORT).show();
        new AsyncMakeMenu(bentoPlan, bentoAdapter, mainListView, getActivity()).execute(null, null);

        listViewHeader.setVisibility(View.GONE);
        mainListView.removeHeaderView(listViewHeader);

        SharedPreferences shared = getActivity().getSharedPreferences(filename, Context.MODE_PRIVATE);
        PreferenceUtil.write(shared, countKey, count);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMenuGenerateListener {
        // TODO: Update argument type and name
        public void onMenuGenerate();
    }
}
