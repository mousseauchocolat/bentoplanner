package org.ict.pck.bentoplanner.img;

/**
 * カラーコード用enum
 * @author ringoh72
 */
public enum ColorName {
	RED,
	GREEN,
	YELLOW,
	OTHER
}
