package org.ict.pck.bentoplanner.menu;

import android.graphics.Bitmap;
import android.util.Log;

import org.ict.pck.bentoplanner.img.ColorName;
import org.ict.pck.bentoplanner.img.ImgprocEngine;
import org.ict.pck.bentoplanner.packing.PackingPattern;
import org.ict.pck.bentoplanner.packing.PackingPatternSelector;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by ic131240 on 2015/09/08.
 */
public class DailyMenu {
	public static final int MIN_MENU_NUM = 3;
	public static final int MAX_INCREASE = 2;
	public static final int MAX_FAIL = 16;

	private int menuNum;
	private List<Ingredient> ingredients;
	private Set<String> genreSet;
	private PackingPattern packingPattern;
	final private Date date;

	public DailyMenu(Date date, List<Ingredient> tabooList) throws Exception {
		this.date = date;
		Random random = new Random();
		this.menuNum = DailyMenu.MIN_MENU_NUM + random.nextInt(DailyMenu.MAX_INCREASE + 1);
		this.ingredients = new ArrayList<Ingredient>();
		this.genreSet = new HashSet<String>();
		int missCount = 0;
		while (missCount < DailyMenu.MAX_FAIL && !this.createMenu(tabooList)){
			missCount++;
		}
		if (DailyMenu.MAX_FAIL <= missCount){
			throw new Exception("Failed to create menu");
		}
	}

	public DailyMenu(Date date, List<Ingredient> ingredients, int menuNum){
		this.date = date;
		this.ingredients = ingredients;
		this.menuNum = menuNum;
		this.genreSet = new HashSet<String>();
		for (Ingredient i : ingredients){
			this.genreSet.addAll(i.getGenreList());
		}
	}

	public List<Ingredient> getIngredients(){
		return this.ingredients;
	}

	public PackingPattern getPackingPattern(){
		return this.packingPattern;
	}

	public Date getDate(){
		return this.date;
	}

	public Bitmap getImage() {
		return this.packingPattern.getBitmap();
	}

	public int getMenuNum(){
		return this.menuNum;
	}

	public void setImage(Bitmap bitmap){
		this.packingPattern = new PackingPattern(bitmap, this.packingPattern.getPoints());
	}

	public boolean createMenu(List<Ingredient> tabooList){
		IngredientSelector selector = IngredientSelector.getInstance();
		List<Ingredient> frozenUseUpIngredients = selector.getFrozenUseUpIngredients();
		List<Ingredient> useUpIngredients = selector.getUseUpIngredients();
		List<Ingredient> loveIngredients  = selector.getLoveIngredients();
		try {
			ColorName[] color = new ColorName[this.menuNum];
			int red, yellow, green;

			red = this.setColor(color, ColorName.RED);
			Ingredient ingredient = selector.select(Category.valueOf(red), this.genreSet, tabooList, ColorName.RED);
			this.addIngredient(ingredient);
			yellow = this.setColor(color, ColorName.YELLOW);
			ingredient = selector.select(Category.valueOf(yellow), this.genreSet, tabooList, ColorName.YELLOW);
			this.addIngredient(ingredient);
			green = this.setColor(color, ColorName.GREEN);
			ingredient = selector.select(Category.valueOf(green), this.genreSet, tabooList, ColorName.GREEN);
			this.addIngredient(ingredient);

			for (int i = 0; i < this.menuNum; i++){
				if (color[i] != null){
					continue;
				}
				ingredient = selector.select(Category.valueOf(i), this.genreSet, tabooList, null);
				this.addIngredient(ingredient);
			}

			for (Ingredient i : this.ingredients){
				if (i.getCategory() == Category.DESSERT){
					this.ingredients.remove(i);
					this.ingredients.add(i);
					break;
				}
			}
		} catch (Exception e){
			selector.setFrozenUseUpIngredients(frozenUseUpIngredients);
			selector.setUseUpIngredients(useUpIngredients);
			selector.setLoveIngredients(loveIngredients);
			for (Ingredient i : this.ingredients){
				tabooList.remove(i);
			}
			this.ingredients.clear();
			this.genreSet.clear();
			return false;
		}
		return true;
	}

	public void paint(){
		if (this.packingPattern == null){
			this.packingPattern = PackingPatternSelector.getInstance().select(this.menuNum);
		}
		Bitmap bitmap = ImgprocEngine.fill(this.packingPattern, this.ingredients);
		this.packingPattern = new PackingPattern(bitmap, this.packingPattern.getPoints());
		Log.i("Menu", "Painted a bento successfully");
	}

	public void changeToLeftover(int pos, RepaintCallback callback){
		Ingredient ingredient = this.ingredients.get(pos);
		this.genreSet.removeAll(ingredient.getGenreList());
		ingredient = IngredientSelector.getInstance().getLeftover();
		this.ingredients.set(pos, ingredient);
		this.genreSet.addAll(ingredient.getGenreList());
		Log.i("Menu", "Changed to " + ingredient.getName());
		this.paint();
		callback.onRepaint(this.getImage());
	}

	public void change(int pos, RepaintCallback callback){
		IngredientSelector selector = IngredientSelector.getInstance();
		Ingredient ingredient = this.ingredients.get(pos);
		Category category = ingredient.getCategory();
		this.genreSet.removeAll(ingredient.getGenreList());
		try {
			ingredient = selector.select(category, this.genreSet, new ArrayList<Ingredient>(), ColorName.OTHER);
		} catch (IllegalArgumentException e){
			e.printStackTrace();
		}
		this.ingredients.set(pos, ingredient);
		this.genreSet.addAll(ingredient.getGenreList());
		Log.i("Menu", "Changed to " + ingredient.getName());
		this.paint();
		callback.onRepaint(this.getImage());
	}

	private int setColor(ColorName[] color, ColorName c){
		Random random = new Random();
		int pos = random.nextInt(this.menuNum);
		while (color[pos] != null){
			pos = (pos + 1) % this.menuNum;
		}
		color[pos] = c;
		return pos;
	}

	private void addIngredient(Ingredient ingredient){
		switch (ingredient.getCategory()){
			case MAIN:
				this.ingredients.add(0, ingredient);
				break;
			default:
				this.ingredients.add(ingredient);
		}
		this.genreSet.addAll(ingredient.getGenreList());
	}

	public interface RepaintCallback {
		void onRepaint(Bitmap bitmap);
	}
}
