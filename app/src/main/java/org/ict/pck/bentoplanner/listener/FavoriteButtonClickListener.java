package org.ict.pck.bentoplanner.listener;

import android.content.res.Resources;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.menu.Ingredient;
import org.ict.pck.bentoplanner.menu.IngredientSelector;

/**
 * Created by mi141320 on 2015/09/18.
 */
public class FavoriteButtonClickListener implements View.OnClickListener {
	private View view;
	private Ingredient ingredient;
	private boolean favFlag;

	public FavoriteButtonClickListener(View convertView, Ingredient ingredient, Resources resources, boolean favFlag){
		this.view = convertView;
		this.ingredient = ingredient;
		this.favFlag = favFlag;
	}

	@Override
	public void onClick(View view) {

		IngredientSelector ingredientSelector = IngredientSelector.getInstance();
		if (!favFlag) {
			Toast toast = Toast.makeText(this.view.getContext(), "お気に入り登録しました。", Toast.LENGTH_SHORT);
			toast.show();
			((ImageButton)view).setImageResource(R.drawable.star_p);
			this.ingredient.setLove(true);
			ingredientSelector.addLoveIngredient(ingredient);
			favFlag = true;
		}
		else {
			Toast toast = Toast.makeText(this.view.getContext(), "お気に入りを取り消しました。", Toast.LENGTH_SHORT);
			toast.show();
			((ImageButton) view).setImageResource(R.drawable.star);
			this.ingredient.setLove(false);
			ingredientSelector.removeLoveIngredient(this.ingredient);
			favFlag = false;
		}

	}
}
