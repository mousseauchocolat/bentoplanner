package org.ict.pck.bentoplanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;

import com.sh1r0.caffe_android_demo.CaffeMobile;

import org.ict.pck.bentoplanner.menu.AllMenu;
import org.ict.pck.bentoplanner.menu.IngredientSelector;
import org.ict.pck.bentoplanner.packing.PackingPatternSelector;
import org.ict.pck.bentoplanner.utils.Utils;
import org.opencv.android.OpenCVLoader;

public class SplashActivity extends ActionBarActivity {

	static {
		System.loadLibrary("caffe");
		System.loadLibrary("caffe_jni");
		OpenCVLoader.initDebug();
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

		new Thread(){
			@Override
			public void run(){
				IngredientSelector selector = IngredientSelector.getInstance();
				selector.load(SplashActivity.this.getApplicationContext());
			}
		}.start();
		new Thread(){
			@Override
			public void run(){
				PackingPatternSelector selector = PackingPatternSelector.getInstance();
				selector.load(SplashActivity.this.getAssets());
			}
		}.start();
		new Thread(){
			@Override
			public void run(){
				CaffeMobile caffeMobile = CaffeMobile.getInstance();
				caffeMobile.enableLog(true);
				String dir = "/sdcard/caffe_mobile/bentoplanner/";
				caffeMobile.loadModel(dir + "deploy_mobile.prototxt", dir + "bentonet.caffemodel");
			}
		}.start();
		AllMenu allMenu = AllMenu.getInstance();
		while (allMenu.isEmpty()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		android.os.Handler handler = new android.os.Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(SplashActivity.this, MainActivity.class);
				SplashActivity.this.startActivity(intent);
				SplashActivity.this.finish();
			}
		}, 1000);
	}

	/*
	@Override
	public void onResume(){
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, new BaseLoaderCallback(this) {
			@Override
			public void onManagerConnected(int status) {
				switch (status) {
					case SUCCESS:
						Log.i("OpenCV", "Loaded OpenCV successfully");
						break;
					default:
						super.onManagerConnected(status);
						break;
				}
			}
		});
	}
	*/
}
