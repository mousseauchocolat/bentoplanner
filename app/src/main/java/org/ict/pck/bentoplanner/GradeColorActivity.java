package org.ict.pck.bentoplanner;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import org.ict.pck.bentoplanner.fragment.TakeAfterFragment;
import org.ict.pck.bentoplanner.fragment.TakeBeforeFragment;

public class GradeColorActivity extends Activity
		implements TakeBeforeFragment.OnFragmentBeforeListener,
		TakeAfterFragment.OnFragmentAfterListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grade_color);

		TakeBeforeFragment fragment = TakeBeforeFragment.newInstance(this);
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.take_before_container, fragment);
		ft.addToBackStack("tag");
		ft.commit();


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		//noinspection SimplifiableIfStatement

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onFragmentBefore(int eventType) {
		switch (eventType){
			case 0:
				this.finish();
				break;
			case 1:
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.take_before_container, TakeAfterFragment.newInstance());
				ft.addToBackStack(null);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				ft.commit();
				break;
			default:
				break;
		}
	}

	@Override
	public void onFragmentAfter(int eventType) {
		switch (eventType){
			case 0:
				FragmentManager fm = getFragmentManager();
				fm.popBackStack();
				break;
			case 1:
				Intent intent = new Intent(this, GradeResultActivity.class);
				this.startActivity(intent);
				overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_right);
				break;
			case 2:
				finish();
				break;
			default:
				break;
		}
	}
}
