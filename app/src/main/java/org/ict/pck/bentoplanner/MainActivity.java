package org.ict.pck.bentoplanner;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import org.ict.pck.bentoplanner.adapter.BentoFragmentPagerAdapter;
import org.ict.pck.bentoplanner.fragment.BentoFragment;
import org.ict.pck.bentoplanner.listener.TransitionFromHomeListener;
import org.ict.pck.bentoplanner.listener.TransitionScreenListener;
import org.ict.pck.bentoplanner.provider.WeekActionProvider;
import org.ict.pck.bentoplanner.utils.Utils;


public class MainActivity extends ActionBarActivity implements BentoFragment.OnMenuGenerateListener {
	private BentoFragmentPagerAdapter adapter;
	private int fragmentPos = 1;
	private ViewPager viewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		this.getSupportActionBar().setTitle(Utils.getWeekTitle(1));

		if (savedInstanceState == null) {
			FragmentManager manager = getSupportFragmentManager();
			viewPager = (ViewPager) findViewById(R.id.view_pager);

			adapter = new BentoFragmentPagerAdapter(manager);
			viewPager.setAdapter(adapter);
			viewPager.setCurrentItem(1, false);
			viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				@Override
				public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				}

				@Override
				public void onPageSelected(int position) {
					fragmentPos = position;
					MainActivity.this.getSupportActionBar().setTitle(Utils.getWeekTitle(position));
				}

				@Override
				public void onPageScrollStateChanged(int state) {
				}
			});

			Button gradeButton = (Button) findViewById(R.id.grade_color_button);
			gradeButton.setOnClickListener(new TransitionScreenListener(this, GradeColorActivity.class));

			Button buyListButton = (Button) findViewById(R.id.view_buy_list_button);
			buyListButton.setOnClickListener(new TransitionFromHomeListener(this, BuyListActivity.class));
		}
	}

	@Override
	public void onStart(){
		super.onStart();
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getAction()==KeyEvent.ACTION_DOWN) {
			switch (event.getKeyCode()) {
				case KeyEvent.KEYCODE_BACK:
					return true;
			}
		}

		return super.dispatchKeyEvent(event);

		/*DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

		SharedPreferences preferences = this.getSharedPreferences(dateFormat.format(new Date()), MODE_PRIVATE);
		for (int i = 0; i < WeeklyMenu.BENTO_DAY_NUM; i++){
			Log.v("Preferences", "Day" + (i + 1));
			int menuNum = preferences.getInt("Day" + (i + 1), 0);
			Log.v("Preferences", "menuNum = " + menuNum);
			for (int j = 1; j <= menuNum; j++){
				String name = preferences.getString("Day" + (i + 1) + j, "");
				Log.v("Preferences", "ingredient : " + name);
			}
		}*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		/*MenuItem menuItem = menu.add("今週");
		menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menuItem.setIcon(R.drawable.this_week_button);
		menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				return false;
			}
		});*/
		getMenuInflater().inflate(R.menu.menu_main, menu);
		menu.findItem(R.id.this_week).setActionProvider(new WeekActionProvider(this));

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		/*}String title = (String)item.getTitle();
		if (title.equals("今週")){
			Log.v("Menu", "This week");
			return true;
		}*/
		if (item.getItemId() == R.id.this_week){
			viewPager.setCurrentItem(1, true);
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onMenuGenerate() {
		/*if (adapter.getCount() < 3 && AllMenu.getInstance().getWeeklyMenus().size() > 0) {
			adapter.addWeekly();
		}
		Log.v("getCount", adapter.getCount() + "");
		Log.v("size", AllMenu.getInstance().getWeeklyMenus().size() + "");
		*/
	}
}
