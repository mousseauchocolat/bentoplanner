package org.ict.pck.bentoplanner.menu;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 全メニュー管理用クラス
 * Created by ringoh72 on 2015/10/24.
 */
public class AllMenu {
	private List<WeeklyMenu> weeklyMenuList;

	private static AllMenu instance;

	static {
		AllMenu.instance = new AllMenu();
	}

	private AllMenu(){
		this.weeklyMenuList = new ArrayList<WeeklyMenu>();
	}

	/**
	 * インスタンスを取得する
	 * @return このクラスのインスタンス
	 */
	public static AllMenu getInstance(){
		return AllMenu.instance;
	}

	/**
	 * 1週間分のメニューを追加する
	 * @param weeklyMenu 1週間分のデータ
	 */
	public void addMenu(WeeklyMenu weeklyMenu){
		this.weeklyMenuList.add(weeklyMenu);
		IngredientSelector selector = IngredientSelector.getInstance();
		for (DailyMenu dailyMenu : weeklyMenu.getDailyMenus()){
			dailyMenu.paint();
			for (Ingredient ingredient : dailyMenu.getIngredients()){
				selector.addUseUpList(ingredient);
			}
		}
		Log.i("Menu", "Added new weekly menu");
	}

	/**
	 * 1週間分のメニューを追加する
	 * @param startDate メニュー作成開始日
	 * @param context アプリのコンテキスト
	 */
	public void addMenu(Date startDate, Context context){
		this.addMenu(new WeeklyMenu(startDate, context));
	}

	/**
	 * 全メニューのリストを取得する
	 * @return 作られた全メニューのリスト
	 */
	public List<WeeklyMenu> getWeeklyMenus(){
		return this.weeklyMenuList;
	}

	/**
	 * 指定した週のメニューリストを取得する
	 * @param pos 週番号
	 * @return メニューリスト
	 * @see #getLastWeeklyMenu()
	 */
	public WeeklyMenu getWeeklyMenu(int pos){
		return this.weeklyMenuList.get(pos);
	}

	/**
	 * 最後に作ったメニューのリストを取得する
	 * @return 最後に作ったメニューのリスト
	 * @see #getWeeklyMenu(int)
	 */
	public WeeklyMenu getLastWeeklyMenu(){
		int size = this.weeklyMenuList.size();
		return this.weeklyMenuList.get(size - 1);
	}

	/**
	 * メニューが空かどうか調べる
	 * @return 空ならtrue, 空でないならfalse
	 */
	public boolean isEmpty(){
		return this.weeklyMenuList.isEmpty();
	}
}
