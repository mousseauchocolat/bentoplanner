package org.ict.pck.bentoplanner.menu;

import org.ict.pck.bentoplanner.img.ColorName;
import org.ict.pck.bentoplanner.img.ColorConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Ingredient {
	private String name;
	private Category category;
	private ColorName color;
	private double[] colorCode;
	private List<Material> materialList;
	private List<String> genreList;
	private boolean loved;

	public Ingredient(JSONObject jsonObject){
		try {
			//名前
			String[] tmpName = jsonObject.getString("name").split("_");
			if (tmpName.length == 1){
				this.name = tmpName[0];
			}
			else {
				this.name = String.format("冷凍食品[%s]\n%s", tmpName[0], tmpName[1]);
			}

			//カテゴリ
			String categoryString = jsonObject.getString("category");
			if (categoryString.equals("主菜")){
				this.category = Category.MAIN;
			}
			else if (categoryString.equals("副菜")){
				this.category = Category.SUB;
			}
			else if (categoryString.equals("デザート")){
				this.category = Category.DESSERT;
			}
			else {
				this.category = Category.LEFTOVER;
			}

			//色
			this.color = ColorConverter.valueOf(jsonObject.getString("color"));
			int colorNum = Integer.valueOf(jsonObject.getString("color_code").substring(1), 16);
			this.colorCode = ColorConverter.rgbToArray(colorNum);

			//材料
			JSONArray materialArray = jsonObject.getJSONArray("material");
			this.materialList = new ArrayList<Material>();
			for (int i = 0; i < materialArray.length(); i++){
				JSONObject material = materialArray.getJSONObject(i);
				materialList.add(new Material(material.getString("name"), material.getString("volume")));
			}

			//ジャンル
			JSONArray genreArray = jsonObject.getJSONArray("genre");
			this.genreList = new ArrayList<String>();
			for (int i = 0; i < genreArray.length(); i++){
				genreList.add(genreArray.getString(i));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.loved = false;
	}

	public void setLove(boolean loved){
		this.loved = loved;
	}

	public String getName(){
		return this.name;
	}

	public Category getCategory(){
		return this.category;
	}

	public ColorName getColor(){
		return this.color;
	}

	public double[] getColorCode(){
		return this.colorCode.clone();
	}

	public List<Material> getMaterialList(){
		return this.materialList;
	}

	public List<String> getGenreList(){
		return this.genreList;
	}

	public boolean isLoved(){
		return this.loved;
	}

	public boolean hasGenre(String genre){
		return this.genreList.contains(genre);
	}

	public boolean hasGenre(Set<String> genreSet){
		for (String s : genreSet){
			if (this.hasGenre(s)){
				return true;
			}
		}
		return false;
	}

	public boolean isColor(ColorName colorName){
		return this.color == colorName;
	}
}
