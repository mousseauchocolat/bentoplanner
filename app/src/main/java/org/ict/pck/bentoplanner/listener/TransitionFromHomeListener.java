package org.ict.pck.bentoplanner.listener;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import org.ict.pck.bentoplanner.menu.AllMenu;

/**
 * Created by ringoh72 on 2015/10/06.
 */
public class TransitionFromHomeListener extends TransitionScreenListener {
	public TransitionFromHomeListener(Activity activity, Class clazz){
		super(activity, clazz);
	}

	@Override
	public void onClick(View view){
		if (AllMenu.getInstance().getWeeklyMenus().size() <= 1){
			Toast.makeText(view.getContext(), "メニューを作成してください", Toast.LENGTH_SHORT).show();
			return;
		}
		super.onClick(view);
	}
}
