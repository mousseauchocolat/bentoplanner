package org.ict.pck.bentoplanner.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import org.ict.pck.bentoplanner.fragment.BentoFragment;
import org.ict.pck.bentoplanner.fragment.FirstBentoFragment;

/**
 * Created by mi141320 on 2015/10/25.
 */
public class BentoFragmentPagerAdapter extends FragmentPagerAdapter{
    private int fragmentCount = 2;
    private BentoFragment mCurrentFragment;

    public BentoFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FirstBentoFragment.newInstance();
            case 1:
                return BentoFragment.newInstance(position);
        }

        return BentoFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return fragmentCount;
    }

    public void addWeekly() {
        fragmentCount++;
        this.notifyDataSetChanged();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object){
        if (position != 1){
            return;
        }
        if (mCurrentFragment != object) {
            mCurrentFragment = (BentoFragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public BentoFragment getCurrentFragment() {
        return mCurrentFragment;
    }
}

