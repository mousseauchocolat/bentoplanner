package org.ict.pck.bentoplanner.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

/**
 * Created by mi141320 on 2015/08/18.
 */
public class TransitionScreenListener implements View.OnClickListener{
    Activity activity;
    Class clazz;

    public TransitionScreenListener(Activity activity, Class clazz){
        this.activity = activity;
        this.clazz = clazz;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this.activity, this.clazz);
        activity.startActivity(intent);
    }
}
