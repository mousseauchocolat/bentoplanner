package org.ict.pck.bentoplanner.menu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ic131240 on 2015/09/04.
 */
public class Material {
	public static Pattern numPattern;

	private String name;
	private double volume;
	private String counterSuffix;

	static {
		Material.numPattern = Pattern.compile("[\\.\\d]+");
	}

	public Material(String name, double volume, String counterSuffix){
		String[] tmpName = name.split("_");
		if (tmpName.length == 1){
			this.name = tmpName[0];
		}
		else {
			this.name = String.format("冷凍食品[%s]\n%s", tmpName[0], tmpName[1]);
		}
		this.volume = volume;
		this.counterSuffix = counterSuffix;
	}

	public Material(String name, String s){
		String[] tmpName = name.split("_");
		if (tmpName.length == 1){
			this.name = tmpName[0];
		}
		else {
			this.name = String.format("冷凍食品[%s]\n%s", tmpName[0], tmpName[1]);
		}
		Matcher matcher = Material.numPattern.matcher(s);
		if (matcher.find()) {
			this.volume = Double.valueOf(matcher.group());
			this.counterSuffix = s.substring(matcher.end());
		}
	}

	public Material(Material material) {
		this(material.getName(), material.getVolume(), material.getCounterSuffix());
	}

	public String getName(){
		return this.name;
	}

	public double getVolume(){
		return this.volume;
	}

	public String getCounterSuffix(){
		return this.counterSuffix;
	}

	public Material sum(Material material){
		if (material == null){
			return this;
		}
		else {
			double sum = this.volume + material.getVolume();
			return new Material(this.name, sum, this.counterSuffix);
		}
	}
}
