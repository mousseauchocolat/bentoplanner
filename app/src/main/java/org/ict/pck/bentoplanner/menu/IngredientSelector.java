package org.ict.pck.bentoplanner.menu;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;

import org.ict.pck.bentoplanner.img.ColorName;
import org.ict.pck.bentoplanner.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 具材を選ぶマン
 * Created by ic131240 on 2015/09/08.
 */
public class IngredientSelector {
	private List<Ingredient> mains;
	private List<Ingredient> subs;
	private List<Ingredient> desserts;
	private Ingredient leftover;

	private List<Ingredient> loveIngredients;
	private List<Ingredient> hateIngredients;
	private List<Ingredient> frozenUseUpIngredients;
	private List<Ingredient> useUpIngredients;

	private boolean ready;

	private Context context;

	private static IngredientSelector instance;
	private static Set<String> allGenre;

	static {
		IngredientSelector.allGenre = new TreeSet<String>();
		IngredientSelector.instance = new IngredientSelector();
	}

	/**
	 * インスタンスを取得する
	 * @return このクラスのインスタンス
	 */
	public static IngredientSelector getInstance(){
		return IngredientSelector.instance;
	}

	private IngredientSelector(){
		this.mains = new ArrayList<Ingredient>();
		this.subs = new ArrayList<Ingredient>();
		this.desserts = new ArrayList<Ingredient>();

		this.hateIngredients = new ArrayList<Ingredient>();
		this.loveIngredients = new ArrayList<Ingredient>();
		this.frozenUseUpIngredients = new ArrayList<Ingredient>();
		this.useUpIngredients = new ArrayList<Ingredient>();

		try {
			JSONObject leftoverJSON = new JSONObject();
			leftoverJSON.put("name", "残り物");
			leftoverJSON.put("category", "残り物");
			leftoverJSON.put("color", "");
			leftoverJSON.put("color_code", "#ffffff");
			leftoverJSON.put("material", new JSONArray());
			leftoverJSON.put("genre", new JSONArray());
			this.leftover = new Ingredient(leftoverJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 具材データを読み込む(外部から呼び出す)
	 * @param context コンテキスト
	 * @see #load(AssetManager, String)
	 */
	public void load(Context context) {
		this.context = context;
		AssetManager assetManager = context.getAssets();
		this.load(assetManager, "main");
		this.load(assetManager, "sub");
		this.load(assetManager, "dessert");
		this.ready = true;
		Log.i("Menu", "Loaded Ingredients successfully");
		Utils.loadSavedData(assetManager);
	}

	/**
	 * #load(AssetManager) 内部で呼び出して各フィールドにJSONを読み込む
	 * @param assetManager アセットマネージャ
	 * @param category 読み込むカテゴリ(main, sub, dessertの3種類)
	 * @see #load(Context)
	 */
	public void load(AssetManager assetManager, String category){
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		try {
			String[] list = assetManager.list("json/" + category);
			for (String filename : list){
				InputStream inputStream = assetManager.open("json/" + category + "/" + filename);
				byte[] bytes = new byte[inputStream.available()];
				inputStream.read(bytes);
				try {
					Ingredient ingredient = new Ingredient(new JSONObject(new String(bytes)));
					ingredients.add(ingredient);
					IngredientSelector.allGenre.addAll(ingredient.getGenreList());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (category.equals("main")){
			this.mains = ingredients;
		}
		else if (category.equals("sub")){
			this.subs = ingredients;
		}
		else if (category.equals("dessert")){
			this.desserts = ingredients;
		}
	}

	/**
	 * 禁止事項に引っかからなかった食材を乱択する
	 * @param category 選ぶ具材のカテゴリ
	 * @param tabooGenres 選んではいけないジャンルの集合
	 * @param tabooIngredients 選んではいけない具材のリスト
	 * @return 選ばれた具材のデータ
	 */
	public Ingredient select(Category category, Set<String> tabooGenres, List<Ingredient> tabooIngredients, ColorName color){
		List<Ingredient> ingredientList = new ArrayList<Ingredient>();
		Random random = new Random();

		for (Ingredient i : this.frozenUseUpIngredients){
			if (this.checkIngredient(i, tabooGenres, tabooIngredients, color)){
				ingredientList.add(i);
			}
		}
		if (!ingredientList.isEmpty()){
			Ingredient ingredient = ingredientList.get(random.nextInt(ingredientList.size()));
			this.frozenUseUpIngredients.remove(ingredient);
			this.loveIngredients.remove(ingredient);
			return ingredient;
		}

		for (Ingredient i : this.useUpIngredients){
			if (this.checkIngredient(i, tabooGenres, tabooIngredients, color)){
				ingredientList.add(i);
			}
		}
		if (!ingredientList.isEmpty()){
			Ingredient ingredient = ingredientList.get(random.nextInt(ingredientList.size()));
			this.useUpIngredients.remove(ingredient);
			this.loveIngredients.remove(ingredient);
			return ingredient;
		}

		for (Ingredient i : this.loveIngredients){
			if (category == i.getCategory() && !tabooIngredients.contains(i) && i.isColor(color)){
				ingredientList.add(i);
			}
		}
		if (!ingredientList.isEmpty()){
			Ingredient ingredient = ingredientList.get(random.nextInt(ingredientList.size()));
			this.loveIngredients.remove(ingredient);
			return ingredient;
		}

		List<Ingredient> ingredients;
		switch (category){
			case MAIN:
				ingredients = this.mains;
				break;
			case SUB:
				ingredients = this.subs;
				break;
			default:
				ingredients = this.desserts;
				break;
		}
		for (Ingredient i : ingredients){
			if (this.checkIngredient(i, tabooGenres, tabooIngredients, color)){
				ingredientList.add(i);
			}
		}

		Ingredient ingredient = ingredientList.get(random.nextInt(ingredientList.size()));
		this.addUseUpList(ingredient);

		return ingredient;
	}

	/**
	 * 残り物の具材データのgetter
	 * @return 残り物の具材データ
	 */
	public Ingredient getLeftover(){
		return this.leftover;
	}

	/**
	 * 名前からおかずデータを探す
	 * @param name 探すおかずの名前
	 * @return おかずデータ(見つからなかった場合はnull)
	 */
	public Ingredient searchIngredient(String name){
		Pattern pattern = Pattern.compile("冷凍食品\\[.+\\]\n");
		for (Ingredient i : this.mains){
			Matcher matcher = pattern.matcher(i.getName());
			matcher.find();
			String ingredientName = matcher.replaceAll("");
			if (name.equals(ingredientName)){
				return i;
			}
		}
		for (Ingredient i : this.subs){
			Matcher matcher = pattern.matcher(i.getName());
			matcher.find();
			String ingredientName = matcher.replaceAll("");
			if (name.equals(ingredientName)){
				return i;
			}
		}
		for (Ingredient i : this.desserts){
			Matcher matcher = pattern.matcher(i.getName());
			matcher.find();
			String ingredientName = matcher.replaceAll("");
			if (name.equals(ingredientName)){
				return i;
			}
		}
		return null;
	}

	/**
	 * 材料を使う具材リストを返す
	 * @param material 材料
	 * @return 具材データ一覧
	 * @see #searchIngredient(ColorName)
	 */
	public List<Ingredient> searchIngredient(Material material){
		List<Ingredient> ingredientList = new ArrayList<Ingredient>();
		for (Ingredient i : this.mains){
			if (i.getMaterialList().contains(material)){
				ingredientList.add(i);
			}
		}
		for (Ingredient i : this.subs){
			if (i.getMaterialList().contains(material)){
				ingredientList.add(i);
			}
		}
		for (Ingredient i : this.desserts){
			if (i.getMaterialList().contains(material)){
				ingredientList.add(i);
			}
		}
		return ingredientList;
	}

	/**
	 * 色から食材を探す
	 * @param colorName 色コード
	 * @return 具材リスト
	 * @see #searchIngredient(Material)
	 */
	public List<Ingredient> searchIngredient(ColorName colorName){
		List<Ingredient> ingredientList = new ArrayList<Ingredient>();
		for (Ingredient i : this.mains){
			if (i.isColor(colorName)){
				ingredientList.add(i);
			}
		}
		for (Ingredient i : this.subs){
			if (i.isColor(colorName)){
				ingredientList.add(i);
			}
		}
		for (Ingredient i : this.desserts){
			if (i.isColor(colorName)){
				ingredientList.add(i);
			}
		}
		return ingredientList;
	}

	/**
	 * おかずを使い切りリストに追加する
	 * @param ingredient 追加するおかずのデータ
	 */
	public void addUseUpList(Ingredient ingredient){
		if (this.frozenUseUpIngredients.contains(ingredient)){
			this.frozenUseUpIngredients.remove(ingredient);
			this.loveIngredients.remove(ingredient);
			return;
		}
		if (this.useUpIngredients.contains(ingredient)){
			this.useUpIngredients.remove(ingredient);
			this.loveIngredients.remove(ingredient);
			return;
		}
		if (this.loveIngredients.contains(ingredient)){
			this.loveIngredients.remove(ingredient);
			return;
		}

		List<Ingredient> useUpSet = new ArrayList<Ingredient>();
		if (ingredient.hasGenre("冷凍食品")) {
			SharedPreferences preferences = this.context.getSharedPreferences("count_save", Context.MODE_PRIVATE);
			int count = preferences.getInt("count_key", 1);
			Material material = ingredient.getMaterialList().get(0);
			int num = (int)(1 / (material.getVolume() * count) - 1);
			for (int i = 0; i < num; i++){
				this.frozenUseUpIngredients.add(ingredient);
			}
		}
		else {
			for (Material material : ingredient.getMaterialList()) {
				List<Ingredient> list = this.searchIngredient(material);
				for (Ingredient i : list) {
					useUpSet.add(i);
				}
			}
			for (Ingredient i : useUpSet) {
				if (i.equals(ingredient)) {
					continue;
				}
				this.useUpIngredients.add(i);
			}
		}
	}

	/**
	 * 嫌いな具材を追加する
	 * @param ingredient 具材データ
	 */
	public void addHateIngredient(Ingredient ingredient){
		if (!this.hateIngredients.contains(ingredient)){
			this.hateIngredients.add(ingredient);
			Log.i("Menu", "Added " + ingredient.getName() + " to hate");
		}
	}

	/**
	 * 好きな具材を追加する
	 * @param ingredient 具材データ
	 */
	public void addLoveIngredient(Ingredient ingredient){
		if (!this.loveIngredients.contains(ingredient)){
			this.loveIngredients.add(ingredient);
			Log.i("Menu", "Added " + ingredient.getName() + " to love");
		}
	}

	/**
	 * 好きな具材を削除する
	 * @param ingredient 具材データ
	 */
	public void removeLoveIngredient(Ingredient ingredient){
		this.loveIngredients.remove(ingredient);
		Log.i("Menu", "Removed " + ingredient.getName() + " from love");
	}

	/**
	 * 具材が選んでも大丈夫か確かめる
	 * @param ingredient 具材データ
	 * @param genreSet 選んではいけないジャンルリスト
	 * @param ingredientList 選んではいけない具材リスト
	 * @param color 選ぶ色(nullの場合はなんでもOK)
	 * @return 具材を選んでも大丈夫か
	 */
	public boolean checkIngredient(Ingredient ingredient, Set<String> genreSet, List<Ingredient> ingredientList, ColorName color){
		if (ingredient.hasGenre(genreSet)){
			return false;
		}
		if (ingredientList.contains(ingredient)){
			return false;
		}
		if (color == ColorName.OTHER || color == null){
			return true;
		}
		return ingredient.isColor(color);
	}

	/**
	 * 冷凍食品の使いきりリストを取得する
	 * @return 冷凍食品の使いきりリスト
	 */
	public List<Ingredient> getFrozenUseUpIngredients(){
		return new ArrayList<Ingredient>(this.frozenUseUpIngredients);
	}

	/**
	 * 冷凍食品以外の使いきりリストを取得する
	 * @return 冷凍食品以外の使いきりリスト
	 */
	public List<Ingredient> getUseUpIngredients(){
		return new ArrayList<Ingredient>(this.useUpIngredients);
	}

	/**
	 * 好きなものリストを取得する
	 * @return 好きなものリスト
	 */
	public List<Ingredient> getLoveIngredients(){
		return new ArrayList<Ingredient>(this.loveIngredients);
	}

	/**
	 * 好きなものリストをセットする
	 * @param loveIngredients 好きなものリスト
	 */
	public void setLoveIngredients(List<Ingredient> loveIngredients){
		this.loveIngredients = loveIngredients;
	}

	/**
	 * 冷凍食品の使いきりリストをセットする
	 * @param frozenUseUpIngredients 冷凍食品の使いきりリスト
	 */
	public void setFrozenUseUpIngredients(List<Ingredient> frozenUseUpIngredients){
		this.frozenUseUpIngredients = frozenUseUpIngredients;
	}

	/**
	 * 冷凍食品以外の使いきりリストをセットする
	 * @param useUpIngredients 冷凍食品以外の使いきりリスト
	 */
	public void setUseUpIngredients(List<Ingredient> useUpIngredients){
		this.useUpIngredients = useUpIngredients;
	}

	/**
	 * ロードが終わったかどうか調べる
	 * @return ロードが終わってたらtrue
	 */
	public synchronized boolean isReady(){
		return this.ready;
	}
}
