package org.ict.pck.bentoplanner.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mi141320 on 2015/10/19.
 */
public class PreferenceUtil {

    public PreferenceUtil(Context context, String filename){
    }

    public static void write(SharedPreferences sharedPreferences, String key, boolean attr){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, attr);
        editor.commit();
    }

    public static void write(SharedPreferences sharedPreferences, String key, float attr){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(key, attr);
        editor.commit();
    }

    public static void write(SharedPreferences sharedPreferences, String key, int attr){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, attr);
        editor.commit();
    }

    public static void write(SharedPreferences sharedPreferences, String key, long attr){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, attr);
        editor.commit();
    }

    public static void write(SharedPreferences sharedPreferences, String key, String attr){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, attr);
        editor.commit();
    }

    public static void delete(SharedPreferences sharedPreferences, String key){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

}
