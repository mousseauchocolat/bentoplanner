package org.ict.pck.bentoplanner.listener;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import org.ict.pck.bentoplanner.adapter.BentoMenuAdapter;
import org.ict.pck.bentoplanner.menu.DailyMenu;

/**
 * Created by mi141320 on 2015/09/21.
 */
public class BentoMenuClickListener implements AdapterView.OnItemClickListener {
    private BentoMenuAdapter bentoMenuAdapter;
    private DailyMenu bento;

    public BentoMenuClickListener(BentoMenuAdapter bentoMenuAdapter, DailyMenu bento){
        this.bentoMenuAdapter = bentoMenuAdapter;
        this.bento = bento;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.bentoMenuAdapter.notifyDataSetChanged();
        //Toast.makeText(bentoMenuAdapter.getContext(), this.bento.getIngredients().get(position).getName(), Toast.LENGTH_SHORT).show();
    }
}
