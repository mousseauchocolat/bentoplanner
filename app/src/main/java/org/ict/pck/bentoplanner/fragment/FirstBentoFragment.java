package org.ict.pck.bentoplanner.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.adapter.BentoAdapter;
import org.ict.pck.bentoplanner.menu.AllMenu;
import org.ict.pck.bentoplanner.menu.DailyMenu;
import org.ict.pck.bentoplanner.menu.WeeklyMenu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FirstBentoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FirstBentoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FirstBentoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    BentoAdapter bentoAdapter;
    ListView mainListView;
    private List<DailyMenu> bentoPlan;

    // TODO: Rename and change types of parameters

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FirstBentoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FirstBentoFragment newInstance() {
        FirstBentoFragment fragment = new FirstBentoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FirstBentoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bento, container, false);
        mainListView = (ListView)rootView.findViewById(R.id.main_list_view);

        mainListView.setAdapter(bentoAdapter);
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        bentoPlan = new ArrayList<DailyMenu>();
        WeeklyMenu thisWeek = AllMenu.getInstance().getWeeklyMenu(0);
        DailyMenu[] firstDay = thisWeek.getDailyMenus();
        bentoPlan = Arrays.asList(firstDay);
        bentoAdapter = new BentoAdapter(activity, R.layout.bento_layout, bentoPlan);
//        mListener = (OnFragmentInteractionListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
