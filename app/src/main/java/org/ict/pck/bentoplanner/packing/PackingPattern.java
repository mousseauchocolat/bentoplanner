package org.ict.pck.bentoplanner.packing;

import android.graphics.Bitmap;
import android.util.Pair;

import java.util.List;

/**
 * 詰め方のパターン画像を管理する
 * Created by ic131240 on 2015/09/10.
 */
public class PackingPattern {
	private Bitmap bitmap;
	private List<Pair<Integer, Integer>> points;

	public PackingPattern(Bitmap bitmap, List<Pair<Integer, Integer>> points){
		this.bitmap = bitmap;
		this.points = points;
	}

	public Bitmap getBitmap(){
		return this.bitmap;
	}

	public List<Pair<Integer, Integer>> getPoints(){
		return this.points;
	}

	public void setBitmap(Bitmap bitmap){
		this.bitmap = bitmap;
	}
}
