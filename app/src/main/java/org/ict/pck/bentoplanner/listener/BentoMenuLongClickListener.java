package org.ict.pck.bentoplanner.listener;

import android.view.View;
import android.widget.AdapterView;
import android.widget.PopupMenu;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.adapter.BentoMenuAdapter;
import org.ict.pck.bentoplanner.menu.DailyMenu;

/**
 * Created by mi141320 on 2015/09/21.
 */
public class BentoMenuLongClickListener implements AdapterView.OnItemLongClickListener {
    private DailyMenu.RepaintCallback callback;
    private BentoMenuAdapter bentoMenuAdapter;
    private DailyMenu bento;

    public BentoMenuLongClickListener(DailyMenu.RepaintCallback callback, BentoMenuAdapter bentoMenuAdapter, DailyMenu bento){
        this.callback = callback;
        this.bentoMenuAdapter = bentoMenuAdapter;
        this.bento = bento;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        PopupMenu popupMenu = new PopupMenu(this.bentoMenuAdapter.getContext(), view);
        popupMenu.getMenuInflater().inflate(R.menu.menu_question_popup, popupMenu.getMenu());
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenuClickListener(callback, bentoMenuAdapter, bento, position));
        return false;
    }
}
