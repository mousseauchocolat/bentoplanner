package org.ict.pck.bentoplanner.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.adapter.BentoAdapter;
import org.ict.pck.bentoplanner.adapter.BentoMenuAdapter;
import org.ict.pck.bentoplanner.fixed.MakeFixedListView;
import org.ict.pck.bentoplanner.listener.BentoMenuClickListener;
import org.ict.pck.bentoplanner.listener.BentoMenuLongClickListener;
import org.ict.pck.bentoplanner.menu.DailyMenu;
import org.ict.pck.bentoplanner.menu.WeeklyMenu;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by mi141320 on 2015/09/18.
 */
public class BentoLayout extends LinearLayout {
    TextView calendarView;
    ImageView bentoImageView;
    ListView bentoMenuView;
    final private DateFormat formatter;

    public BentoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.formatter = new SimpleDateFormat("yy/MM/dd (E)");
    }

    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();

        this.calendarView = (TextView)findViewById(R.id.calendar_text);
        this.bentoImageView = (ImageView)findViewById(R.id.bento_image);
        this.bentoMenuView = (ListView)findViewById(R.id.bento_menu_list);
    }

    public void bindView(final BentoAdapter bentoAdapter, final DailyMenu dailyMenu) {
        this.calendarView.setText(this.formatter.format(dailyMenu.getDate()));
        this.bentoImageView.setImageBitmap(dailyMenu.getImage());

        BentoMenuAdapter bentoMenuAdapter = new BentoMenuAdapter(getContext(), R.layout.bento_menu_layout, dailyMenu.getIngredients());
        this.bentoMenuView.setOnItemClickListener(new BentoMenuClickListener(bentoMenuAdapter, dailyMenu));
        DailyMenu.RepaintCallback callback = new DailyMenu.RepaintCallback() {
            @Override
            public void onRepaint(Bitmap bitmap) {
                BentoLayout.this.bentoImageView.setImageBitmap(bitmap);
                bentoAdapter.notifyDataSetChanged();
                Log.v("Menu", "Repainted");
            }
        };
        this.bentoMenuView.setOnItemLongClickListener(new BentoMenuLongClickListener(callback, bentoMenuAdapter, dailyMenu));
        this.bentoMenuView.setAdapter(bentoMenuAdapter);
        MakeFixedListView.makeFixedListView(bentoMenuView, 0);
    }
}
