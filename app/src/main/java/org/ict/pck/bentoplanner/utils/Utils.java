package org.ict.pck.bentoplanner.utils;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import org.ict.pck.bentoplanner.menu.AllMenu;
import org.ict.pck.bentoplanner.menu.DailyMenu;
import org.ict.pck.bentoplanner.menu.Ingredient;
import org.ict.pck.bentoplanner.menu.IngredientSelector;
import org.ict.pck.bentoplanner.menu.WeeklyMenu;
import org.ict.pck.bentoplanner.packing.PackingPatternSelector;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ic131240 on 2015/09/04.
 * アプリ全体で共有する変数とか関数を入れておく
 */
public class Utils {
	/**
	 * 画像保存パスを取得
	 * @return 画像保存パス
	 */
	public static String getSaveDir(){
		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/bentoplanner";
	}

	/**
	 * 今日の画像のファイル名を取得
	 * @return 今日の画像のファイル名
	 */
	public static String getTodayImgPath(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		return sf.format(cal.getTime()) + ".jpg";
	}

	/**
	 * 今日の画像のファイル名をフルパスで取得
	 * @return 今日の画像のファイル名
	 */
	public static String getTodayImgFullPath(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		return getSaveDir() + "/" + sf.format(cal.getTime()) + ".jpg";
	}

	public static boolean sdcardWriteReady(){
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}

	public synchronized static boolean loadSavedData(AssetManager assetManager){
		IngredientSelector ingredientSelector = IngredientSelector.getInstance();
		PackingPatternSelector packingPatternSelector = PackingPatternSelector.getInstance();

		if (!ingredientSelector.isReady() || !packingPatternSelector.isReady()){
			return false;
		}
		try {
			InputStream inputStream = assetManager.open("saved_data.txt");
			byte[] bytes = new byte[inputStream.available()];
			inputStream.read(bytes);
			String[] days = new String(bytes).split("\\d日目\\s+");
			DailyMenu[] dailyMenus = new DailyMenu[WeeklyMenu.BENTO_DAY_NUM];
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.WEEK_OF_MONTH, -1);
			for (int i = 0; i < WeeklyMenu.BENTO_DAY_NUM; i++, calendar.add(Calendar.DAY_OF_WEEK, 1)){
				Log.i("Saved", "Day " + (i + 1));
				switch (calendar.get(Calendar.DAY_OF_WEEK)){
					case Calendar.SUNDAY:
						calendar.add(Calendar.DAY_OF_WEEK, 1);
						break;
					case Calendar.SATURDAY:
						calendar.add(Calendar.DAY_OF_WEEK, 2);
						break;
				}
				List<Ingredient> ingredients = new ArrayList<Ingredient>();
				for (String name : days[i + 1].split("\\s+")){
					Ingredient ingredient = ingredientSelector.searchIngredient(name);
					ingredients.add(ingredient);
					Log.i("Saved", ingredient.getName());
				}
				dailyMenus[i] = new DailyMenu(calendar.getTime(), ingredients, ingredients.size());
			}
			AllMenu allMenu = AllMenu.getInstance();
			allMenu.addMenu(new WeeklyMenu(dailyMenus));

			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 行列を画像に変換して保存する
	 * @param filename ファイル名(拡張子含む)
	 * @param mat 画像データ行列
	 */
	public static void saveMat(String filename, Mat mat){
		Highgui.imwrite(Utils.getSaveDir() + filename, mat);
	}

	public static Date getStartDate(int position){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.WEEK_OF_MONTH, position - 1);
		switch (calendar.get(Calendar.DAY_OF_WEEK)){
			case Calendar.SUNDAY:
				calendar.add(Calendar.DAY_OF_WEEK, 1);
				break;
			case Calendar.SATURDAY:
				calendar.add(Calendar.DAY_OF_WEEK, 2);
				break;
		}

		return calendar.getTime();
	}

	public static String getWeekTitle(int position){
		DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd/(E)");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.WEEK_OF_MONTH, position - 1);
		switch (calendar.get(Calendar.DAY_OF_WEEK)){
			case Calendar.SUNDAY:
				calendar.add(Calendar.DAY_OF_WEEK, 1);
				break;
			case Calendar.SATURDAY:
				calendar.add(Calendar.DAY_OF_WEEK, 2);
				break;
		}

		String title = dateFormat.format(calendar.getTime());
		for (int i = 0; i < 4; i++) {
			calendar.add(Calendar.DAY_OF_WEEK, 1);
			switch (calendar.get(Calendar.DAY_OF_WEEK)) {
				case Calendar.SUNDAY:
					calendar.add(Calendar.DAY_OF_WEEK, 1);
					break;
				case Calendar.SATURDAY:
					calendar.add(Calendar.DAY_OF_WEEK, 2);
					break;
			}
		}
		title += "～" + dateFormat.format(calendar.getTime());

		return title;
	}
}
