package org.ict.pck.bentoplanner.menu;

import android.util.Pair;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ic131240 on 2015/09/09.
 */
public class BuyList {
	Map<String, Pair<Double, String>> buyMap;
	private int num;

	public BuyList(List<WeeklyMenu> weeklyMenus){
		this(weeklyMenus, 1);
	}

	public BuyList(List<WeeklyMenu> weeklyMenus, int num){
		this.buyMap = new TreeMap<String, Pair<Double, String>>();
		this.num = num;

		for (WeeklyMenu weeklyMenu : weeklyMenus) {
			for (DailyMenu dailyMenu : weeklyMenu.getDailyMenus()) {
				for (Ingredient ingredient : dailyMenu.getIngredients()) {
					for (Material material : ingredient.getMaterialList()) {
						String name = material.getName();
						String counterSuffix = material.getCounterSuffix();
						Pair<Double, String> currentPair = this.buyMap.get(name);
						double currentVolume;

						if (currentPair == null) {
							currentVolume = 0;
						} else {
							currentVolume = currentPair.first;
						}
						this.buyMap.put(name, new Pair<Double, String>(material.getVolume() + currentVolume, counterSuffix));
					}
				}
			}
		}

		/*
		for (String s : this.buyMap.keySet()){
			Pair<Double, String> pair = this.buyMap.get(s);
			Log.i("Buy", s + ": " + (int) Math.ceil(pair.first * this.num) + pair.second);
		}
		*/
	}

	public void setNum(int n){
		for (String s : this.buyMap.keySet()){
			Pair<Double, String> pair = this.buyMap.get(s);
			double volume = pair.first / this.num * n;
			this.buyMap.put(s, new Pair<Double, String>(volume, pair.second));
		}
		this.num = n;
	}

	public int getNum() {
		return this.num;
	}

	public Map<String, Pair<Double, String>> getBuyMap() {
		return this.buyMap;
	}
}
