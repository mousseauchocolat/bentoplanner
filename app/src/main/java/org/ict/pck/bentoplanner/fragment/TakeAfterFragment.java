package org.ict.pck.bentoplanner.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.utils.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentAfterListener} interface
 * to handle interaction events.
 * Use the {@link TakeAfterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TakeAfterFragment extends Fragment {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


	// TODO: Rename and change types of parameters

	private OnFragmentAfterListener listener;
	private ImageButton retryButton;
	private ImageButton gradeResultButton;
	private ImageButton goBackHomeButton;
	private ImageView tmpPhotoImage;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @return A new instance of fragment TakeAfterFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static TakeAfterFragment newInstance() {
		TakeAfterFragment fragment = new TakeAfterFragment();
		return fragment;
	}

	public TakeAfterFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_take_after, container, false);
		this.retryButton = (ImageButton)rootView.findViewById(R.id.retry_grade_button);
		this.gradeResultButton = (ImageButton)rootView.findViewById(R.id.grade_result_button);
		this.goBackHomeButton = (ImageButton)rootView.findViewById(R.id.go_back_home_button);

		retryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onButtonPressed(0);
			}
		});
		gradeResultButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onButtonPressed(1);
			}
		});
		goBackHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onButtonPressed(2);
			}
		});

		this.tmpPhotoImage = (ImageView)rootView.findViewById(R.id.tmp_photo_image);
		try {
			InputStream inputStream = new FileInputStream(Utils.getTodayImgFullPath());
			Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
			this.tmpPhotoImage.setImageBitmap(bitmap);
			Log.v("Grade", "Success");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return rootView;
	}

	public void onButtonPressed(int eventType) {
		if (listener != null) {
			listener.onFragmentAfter(eventType);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (activity instanceof TakeBeforeFragment.OnFragmentBeforeListener) {
			listener = (OnFragmentAfterListener) activity;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		listener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentAfterListener {
		public void onFragmentAfter(int eventType);
	}
}
