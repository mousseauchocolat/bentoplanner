package org.ict.pck.bentoplanner.menu;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import org.ict.pck.bentoplanner.adapter.BentoAdapter;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ringoh72 on 2015/10/20.
 */
public class AsyncMakeMenu extends AsyncTask<Void, Void, List<DailyMenu>> {
	List<DailyMenu> bentoPlan;
	BentoAdapter bentoAdapter;
	ListView mainListView;
	Context context;

	public AsyncMakeMenu(List<DailyMenu> bentoPlan, BentoAdapter bentoAdapter, ListView mainListView, Context context){
		this.bentoPlan = bentoPlan;
		this.bentoAdapter = bentoAdapter;
		this.mainListView = mainListView;
		this.context = context;
	}

	public AsyncMakeMenu(DailyMenu[] bentoPlan, BentoAdapter bentoAdapter, ListView mainListView, Context context){
		this.bentoPlan = Arrays.asList(bentoPlan);
		this.bentoAdapter = bentoAdapter;
		this.mainListView = mainListView;
		this.context = context;
	}

	@Override
	protected List<DailyMenu> doInBackground(Void... voids) {
		if (!this.bentoPlan.isEmpty()){
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Date startDate;
		if (AllMenu.getInstance().getWeeklyMenus().size() == 0) {
			startDate = new Date();
		}
		else {
			DailyMenu[] dailyMenus = AllMenu.getInstance().getLastWeeklyMenu().getDailyMenus();
			calendar.setTime(dailyMenus[4].getDate());
			calendar.add(Calendar.DATE, 1);
			startDate = calendar.getTime();
		}
		AllMenu allMenu = AllMenu.getInstance();

		calendar.setTime(startDate);
		allMenu.addMenu(startDate, context);
		DailyMenu[] dailyMenus = allMenu.getLastWeeklyMenu().getDailyMenus();
		return Arrays.asList(dailyMenus);
	}

	@Override
	protected void onPostExecute(List<DailyMenu> menuList){
		//Collections.reverse(menuList);
		if (menuList != null){
			int i = 0;
			for (DailyMenu dailyMenu : menuList){
				bentoPlan.add(i++, dailyMenu);
			}
		}
		bentoAdapter.notifyDataSetChanged();
		// or  mainListView.setSelection(6);
	}
}
