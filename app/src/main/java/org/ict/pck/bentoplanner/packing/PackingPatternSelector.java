package org.ict.pck.bentoplanner.packing;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.Pair;

import org.ict.pck.bentoplanner.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 敷き詰めパターンを選ぶマン
 * Created by ic131240 on 2015/09/10.
 */
public class PackingPatternSelector {
	private List<PackingPattern> three;
	private List<PackingPattern> four;
	private List<PackingPattern> five;

	private boolean ready;

	private static PackingPatternSelector instance;

	static {
		PackingPatternSelector.instance = new PackingPatternSelector();
	}

	private PackingPatternSelector(){
		this.three = new ArrayList<PackingPattern>();
		this.four = new ArrayList<PackingPattern>();
		this.five = new ArrayList<PackingPattern>();
		this.ready = false;
	}

	/**
	 * インスタンスを取得する
	 * @return このクラスのインスタンス
	 */
	public static PackingPatternSelector getInstance(){
		return PackingPatternSelector.instance;
	}

	/**
	 * 敷き詰めパターンを読み込む(外部から呼び出す)
	 * @param assetManager アセットマネージャ
	 * @see #load(AssetManager, String)
	 */
	public void load(AssetManager assetManager){
		PackingPatternSelector.instance.load(assetManager, "three");
		PackingPatternSelector.instance.load(assetManager, "four");
		PackingPatternSelector.instance.load(assetManager, "five");
		this.ready = true;
		Log.i("Packing", "Loaded Patterns successfully");
		Utils.loadSavedData(assetManager);
	}

	/**
	 * 敷き詰めパターンを読み込む(内部で呼び出す)
	 * @param assetManager アセットマネージャ
	 * @param number 読み込む敷き詰めパターンの品数(three, four, fiveの3つ)
	 * @see #load(AssetManager)
	 */
	private void load(AssetManager assetManager, String number){
		List<PackingPattern> packingPatterns = new ArrayList<PackingPattern>();
		try {
			String[] imageNames = assetManager.list("image/" + number);
			String[] pointsNames = assetManager.list("csv/" + number);
			for (int i = 0; i < imageNames.length; i++){
				//データ読み込み(バイナリ)
				InputStream inputStream = assetManager.open("image/" + number + "/" + imageNames[i]);
				Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

				inputStream = assetManager.open("csv/" + number + "/" + pointsNames[i]);
				byte[] bytes = new byte[inputStream.available()];
				inputStream.read(bytes);
				String[] lines = new String(bytes).split("\\s+");
				List<Pair<Integer, Integer>> points = new ArrayList<Pair<Integer, Integer>>();
				for (String line : lines){
					String[] val = line.split(",");
					int[] point = new int[2];
					point[0] = Integer.valueOf(val[1]);
					point[1] = Integer.valueOf(val[0]);
					points.add(new Pair<Integer, Integer>(point[0], point[1]));
				}

				packingPatterns.add(new PackingPattern(bitmap, points));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (number.equals("three")){
			this.three = packingPatterns;
		}
		else if (number.equals("four")){
			this.four = packingPatterns;
		}
		else if (number.equals("five")){
			this.five = packingPatterns;
		}
	}

	/**
	 * 具材数に合わせて詰め方を選ぶ
	 * @param menuNum 具材数
	 * @return 詰め方パターン
	 */
	public PackingPattern select(int menuNum){
		Random random = new Random();
		switch (menuNum){
			case 3:
				return this.three.get(random.nextInt(this.three.size()));
			case 4:
				return this.four.get(random.nextInt(this.four.size()));
			default:
				return this.five.get(random.nextInt(this.five.size()));
		}
	}

	/**
	 * ロードが終わったかどうか調べる
	 * @return ロードが終わってたらtrue
	 */
	public synchronized boolean isReady(){
		return this.ready;
	}
}
