package org.ict.pck.bentoplanner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import org.ict.pck.bentoplanner.menu.DailyMenu;
import org.ict.pck.bentoplanner.view.BentoLayout;

import java.util.List;

/**
 * Created by mi141320 on 2015/09/09.
 */
public class BentoAdapter extends ArrayAdapter<DailyMenu> {
    private LayoutInflater layoutInflater;
    private int resourceId;

    public BentoAdapter(Context context, int textViewResourceId, List<DailyMenu> objects) {
        super(context, textViewResourceId, objects);
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final BentoLayout view;

        if (convertView == null){
            view = (BentoLayout)this.layoutInflater.inflate(this.resourceId, null);
        }
        else {
            view = (BentoLayout)convertView;
        }
        view.bindView(this, getItem(position));

        return view;
    }
}
