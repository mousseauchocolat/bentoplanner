package org.ict.pck.bentoplanner.listener;

import android.app.Activity;
import android.view.View;

/**
 * Created by mi141320 on 2015/08/17.
 */
public class FinishListener implements View.OnClickListener {
	Activity activity;

	public FinishListener(Activity activity){
		this.activity = activity;
	}

	@Override
	public void onClick(View view) {
		this.activity.finish();
	}
}
