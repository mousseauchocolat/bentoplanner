package org.ict.pck.bentoplanner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import org.ict.pck.bentoplanner.R;
import org.ict.pck.bentoplanner.listener.FavoriteButtonClickListener;
import org.ict.pck.bentoplanner.menu.Ingredient;
import org.ict.pck.bentoplanner.view.BentoMenuLayout;

import java.util.List;

/**
 * Created by mi141320 on 2015/09/09.
 */
public class BentoMenuAdapter extends ArrayAdapter<Ingredient> {
    private LayoutInflater layoutInflater;
    private int resourceId;
    private ImageButton favButton;
    private boolean favFlag;

    public BentoMenuAdapter(Context context, int textViewResourceId, List<Ingredient> objects) {
        super(context, textViewResourceId, objects);

        resourceId = textViewResourceId;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        BentoMenuLayout view;

        Ingredient ingredient = getItem(position);

        if (convertView == null){
            view = (BentoMenuLayout)this.layoutInflater.inflate(resourceId, null);

            favButton = (ImageButton)view.findViewById(R.id.fav_button);
            if (!favFlag){
                favButton.setImageResource(R.drawable.star);
            }
            else {
                favButton.setImageResource(R.drawable.star_p);
            }
            favButton.setOnClickListener(new FavoriteButtonClickListener(view, ingredient, view.getResources(), favFlag));
        }
        else {
            view = (BentoMenuLayout)convertView;
        }

        view.bindView(ingredient, position);
        return view;
    }
}
