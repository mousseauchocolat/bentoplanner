package org.ict.pck.bentoplanner;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.sh1r0.caffe_android_demo.CaffeMobile;

import org.ict.pck.bentoplanner.img.ColorName;
import org.ict.pck.bentoplanner.img.ImgprocEngine;
import org.ict.pck.bentoplanner.listener.TransitionScreenListener;
import org.ict.pck.bentoplanner.menu.AllMenu;
import org.ict.pck.bentoplanner.menu.DailyMenu;
import org.ict.pck.bentoplanner.menu.Ingredient;
import org.ict.pck.bentoplanner.menu.IngredientSelector;
import org.ict.pck.bentoplanner.provider.WeekActionProvider;
import org.ict.pck.bentoplanner.utils.Utils;
import org.opencv.core.Mat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;

public class GradeResultActivity extends ActionBarActivity {

	private ImageView photoImage;
	//private Bitmap bitmap;
	private Bitmap[] bitmap;
	private int current;
	private CheckBox logCheck;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grade_result);

		this.photoImage = (ImageView)this.findViewById(R.id.photo_image);
		try {
			InputStream inputStream = new FileInputStream(Utils.getTodayImgFullPath());
			this.bitmap = new Bitmap[4];
			this.bitmap[0] = BitmapFactory.decodeStream(inputStream);
			Mat m = ImgprocEngine.resize(ImgprocEngine.bitmapToMat(this.bitmap[0]), 0.3);
			this.bitmap[0] = ImgprocEngine.matToBitmap(m);
			this.photoImage.setImageBitmap(this.bitmap[0]);
			Mat mat = ImgprocEngine.rgbToHSV(ImgprocEngine.bitmapToMat(this.bitmap[0]));
			this.bitmap[1] = ImgprocEngine.matToBitmap(ImgprocEngine.detect(mat, ColorName.RED));
			this.bitmap[2] = ImgprocEngine.matToBitmap(ImgprocEngine.detect(mat, ColorName.YELLOW));
			this.bitmap[3] = ImgprocEngine.matToBitmap(ImgprocEngine.detect(mat, ColorName.GREEN));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		/*
		this.current = 0;
		this.photoImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				current = (current + 1) % 4;
				GradeResultActivity.this.photoImage.setImageBitmap(GradeResultActivity.this.bitmap[current]);
			}
		});
		*/

		View retryGradeButton = this.findViewById(R.id.retry_grade_button);
		retryGradeButton.setOnClickListener(new TransitionScreenListener(this, GradeColorActivity.class));

		this.logCheck = (CheckBox)this.findViewById(R.id.log_check);
		if (AllMenu.getInstance().getWeeklyMenus().size() <= 1){
			this.logCheck.setEnabled(false);
			this.logCheck.setTextColor(Color.argb(64, 0, 0, 0));
		}
		this.findViewById(R.id.home_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (GradeResultActivity.this.logCheck.isChecked()) {
					AllMenu allMenu = AllMenu.getInstance();
					DailyMenu dailyMenu = allMenu.getWeeklyMenu(1).getDailyMenus()[0];
					dailyMenu.setImage(GradeResultActivity.this.bitmap[0]);
				}
				GradeResultActivity.this.finish();
			}
		});

		final ProgressDialog dialog = ProgressDialog.show(this, "採点中", "しばらくお待ちください…", true);
		new AsyncTask<Void, Integer, List<Integer>>(){
			@Override
			protected List<Integer> doInBackground(Void... voids) {
				Mat mat = new Mat();
				org.opencv.android.Utils.bitmapToMat(GradeResultActivity.this.bitmap[0], mat);
				List<Integer> grade = ImgprocEngine.gradeBento(ImgprocEngine.rgbToHSV(ImgprocEngine.resize(mat, 0.5)));
				grade.add(CaffeMobile.getInstance().predictImage(Utils.getTodayImgFullPath()));
				Log.i("Grading", Integer.toString(grade.get(4)));
				return grade;
			}

			@Override
			protected void onPostExecute(List<Integer> grade){
				/*
				String resultString = "評価: ";
				while (grade-- > 0){
					resultString += "☆";
				}
				*/
				int resultValue = grade.get(4);
				int r = grade.get(0);
				int y = grade.get(1);
				int g = grade.get(2);
				ImageView result = (ImageView)GradeResultActivity.this.findViewById(R.id.result);
				TextView recommend = (TextView)GradeResultActivity.this.findViewById(R.id.recommend_ingredient);
				List<Ingredient> ingredientList = null;
				IngredientSelector ingredientSelector = IngredientSelector.getInstance();
				switch (resultValue){
					case 0:
						ingredientList = ingredientSelector.searchIngredient(ColorName.GREEN);
						break;
					case 1:
						result.setImageResource(R.drawable.star1);
						ingredientList = ingredientSelector.searchIngredient(ColorName.GREEN);
						break;
					case 2:
						result.setImageResource(R.drawable.star2);
						if (r == 0){
							ingredientList = ingredientSelector.searchIngredient(ColorName.RED);
						}
						else if (y == 0){
							ingredientList = ingredientSelector.searchIngredient(ColorName.YELLOW);
						}
						else {
							ingredientList = ingredientSelector.searchIngredient(ColorName.GREEN);
						}
						break;
					case 3:
						result.setImageResource(R.drawable.star3);
						if (r == 0){
							ingredientList = ingredientSelector.searchIngredient(ColorName.RED);
						}
						else if (y == 0){
							ingredientList = ingredientSelector.searchIngredient(ColorName.YELLOW);
						}
						else {
							ingredientList = ingredientSelector.searchIngredient(ColorName.GREEN);
						}
						break;
					case 4:
						result.setImageResource(R.drawable.star4);
						if (r < y && r * 2 < g){
							ingredientList = ingredientSelector.searchIngredient(ColorName.RED);
						}
						else if (y < r && y * 2 < g){
							ingredientList = ingredientSelector.searchIngredient(ColorName.YELLOW);
						}
						else {
							ingredientList = ingredientSelector.searchIngredient(ColorName.GREEN);
						}
						break;
					default:
						result.setImageResource(R.drawable.star5);
						break;
				}
				if (ingredientList != null){
					Ingredient ingredient = ingredientList.get(new Random().nextInt(ingredientList.size()));
					recommend.setText("" + ingredient.getName());
				}
				dialog.dismiss();
			}
		}.execute(null, null);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_grade_result, menu);
		menu.findItem(R.id.home_white).setActionProvider(new WeekActionProvider(this));
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (item.getItemId() == R.id.home_white){
			finish();
		}

		return super.onOptionsItemSelected(item);
	}
}
