package org.ict.pck.bentoplanner;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;

import org.ict.pck.bentoplanner.listener.FinishListener;
import org.ict.pck.bentoplanner.menu.AllMenu;
import org.ict.pck.bentoplanner.menu.BuyList;
import org.ict.pck.bentoplanner.menu.WeeklyMenu;
import org.ict.pck.bentoplanner.provider.WeekActionProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BuyListActivity extends ActionBarActivity {

	NumberPicker personsNumber;
	ListView buyListView;
	BuyList buyList;
	List<Map<String, String>> materialMapList;
	SimpleAdapter simpleAdapter;
	final String filename = "count_save";
	final String countKey = "count_key";
	AllMenu allMenu;
	int count;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		*/
		setContentView(R.layout.activity_buy_list);


		this.findViewById(R.id.buy_list_to_home_button).setOnClickListener(new FinishListener(this));


		SharedPreferences preferences = getSharedPreferences(filename, Context.MODE_PRIVATE);
		count = preferences.getInt(countKey, 1);

		this.allMenu = AllMenu.getInstance();
		Log.v("BuyList", "" + count);
		List<WeeklyMenu> weeklyMenus = new ArrayList<WeeklyMenu>();
		weeklyMenus.add(allMenu.getWeeklyMenu(1));
		this.buyList = new BuyList(weeklyMenus, count);
		this.materialMapList = new ArrayList<Map<String, String>>();
		this.setMaterialMapList();
		this.simpleAdapter = new SimpleAdapter(this.getApplicationContext(),
				this.materialMapList,
				R.layout.buy_list_layout,
				new String[]{"name", "volume"},
				new int[]{R.id.name_text, R.id.volume_text});
		this.buyListView = (ListView)this.findViewById(R.id.buy_list);
		this.buyListView.setAdapter(simpleAdapter);
		this.simpleAdapter.notifyDataSetChanged();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.menu_buy_list, menu);

		getMenuInflater().inflate(R.menu.menu_buy_list, menu);
		menu.findItem(R.id.home_list).setActionProvider(new WeekActionProvider(this));
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.home_list) {
			this.finish();
			//Toast.makeText(this, "moi", Toast.LENGTH_SHORT).show();
		}

		return super.onOptionsItemSelected(item);
	}

	public void setMaterialMapList(){
		Map<String, Pair<Double, String>> map = this.buyList.getBuyMap();
		this.materialMapList.clear();

		for (String s : map.keySet()){
			Map<String, String> materialMap = new HashMap<String, String>();
			materialMap.put("name", s);
			Pair<Double, String> pair = map.get(s);
			materialMap.put("volume", (int) Math.ceil(pair.first) + pair.second);
			this.materialMapList.add(materialMap);
		}
	}
}
