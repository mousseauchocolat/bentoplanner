package org.ict.pck.bentoplanner.menu;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ic131240 on 2015/09/09.
 */
public class WeeklyMenu {
	public static final int BENTO_DAY_NUM = 5;
	public static final int TABOO_SPAN = 2;

	private DailyMenu[] dailyMenus;

	public WeeklyMenu(Date startDate, Context context){
		this.dailyMenus = new DailyMenu[WeeklyMenu.BENTO_DAY_NUM];
		IngredientSelector selector = IngredientSelector.getInstance();
		List<Ingredient> loveIngredients = selector.getLoveIngredients();
		List<Ingredient> frozenUseUpIngredients = selector.getFrozenUseUpIngredients();
		List<Ingredient> useUpIngredients = selector.getUseUpIngredients();
		while (!this.createMenu(startDate)){
			selector.setLoveIngredients(loveIngredients);
			selector.setFrozenUseUpIngredients(frozenUseUpIngredients);
			selector.setUseUpIngredients(useUpIngredients);
		}

		//色塗り
		for (DailyMenu dailyMenu : this.dailyMenus){
			dailyMenu.paint();
		}

		//データ保存
		String name = new SimpleDateFormat("yyyyMMdd").format(startDate);
		SharedPreferences preferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		for (int i = 0; i < BENTO_DAY_NUM; i++){
			int menuNum = this.dailyMenus[i].getMenuNum();
			editor.putInt("Day" + (i + 1), menuNum);

			List<Ingredient> ingredients = this.dailyMenus[i].getIngredients();
			for (int j = 0; j < menuNum; j++){
				editor.putString("Day" + (i + 1) + (j + 1), ingredients.get(j).getName());
			}
		}
		editor.commit();
	}

	public WeeklyMenu(DailyMenu[] dailyMenus){
		this.dailyMenus = dailyMenus;
	}

	public boolean createMenu(Date startDate){
		try {
			List tabooList = new ArrayList<Ingredient>();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			switch (calendar.get(Calendar.DAY_OF_WEEK)){
				case Calendar.SUNDAY:
					calendar.add(Calendar.DATE, 1);
					break;
				case Calendar.SATURDAY:
					calendar.add(Calendar.DATE, 2);
					break;
			}
			for (int i = 0; i < WeeklyMenu.BENTO_DAY_NUM; i++) {
				this.dailyMenus[i] = new DailyMenu(calendar.getTime(), tabooList);
				for (Ingredient ingredient : this.dailyMenus[i].getIngredients()) {
					tabooList.add(ingredient);
				}
				if (WeeklyMenu.TABOO_SPAN <= i) {
					for (Ingredient ingredient : this.dailyMenus[i - WeeklyMenu.TABOO_SPAN].getIngredients()) {
						tabooList.remove(ingredient);
					}
				}
				calendar.add(Calendar.DATE, 1);
				switch (calendar.get(Calendar.DAY_OF_WEEK)){
					case Calendar.SUNDAY:
						calendar.add(Calendar.DATE, 1);
						break;
					case Calendar.SATURDAY:
						calendar.add(Calendar.DATE, 2);
						break;
				}
			}
		} catch (Exception e){
			e.printStackTrace();
			Log.e("Menu", "Failed to create a weekly menu");
			return false;
		}
		return true;
	}

	public DailyMenu[] getDailyMenus(){
		return this.dailyMenus;
	}
}
