package org.ict.pck.bentoplanner.listener;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.hardware.Camera;
import android.provider.MediaStore;
import android.util.Log;

import org.ict.pck.bentoplanner.fragment.TakeBeforeFragment;
import org.ict.pck.bentoplanner.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by ic131240 on 2015/09/01.
 */
public class CameraPictureCallback implements Camera.PictureCallback {

	private TakeBeforeFragment takeBeforeFragment;
	private Activity activity;

	/**
	 * コンストラクタ
	 * @param activity 呼び出したアクティビティ
	 */
	public CameraPictureCallback(TakeBeforeFragment takeBeforeFragment, Activity activity){
		this.takeBeforeFragment = takeBeforeFragment;
		this.activity = activity;
	}

	@Override
	public void onPictureTaken(byte[] bytes, Camera camera) {
		Log.v("Photo", "onPictureTaken");
		if (bytes == null){
			//画像が空ならreturn
			return;
		}
		if (Utils.sdcardWriteReady() == false){
			Log.e("BentoPlanner", "Cannnot write SD card");
		}

		//保存パス
		String saveDir = Utils.getSaveDir();
		File file = new File(saveDir);
		if (file.exists() == false){
			//なければ作る
			if (file.mkdir() == false){
				Log.e("BentoPlanner", "mkdir error");
			}
		}

		String imgPath = Utils.getTodayImgFullPath();
		//String imgPath = Utils.getTodayImgPath();
		//保存する
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(imgPath, false);
			//FileOutputStream fileOutputStream = this.activity.getApplicationContext().openFileOutput(imgPath, Context.MODE_APPEND);
			fileOutputStream.write(bytes);
			fileOutputStream.close();

			/*
			Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			MediaStore.Images.Media.insertImage(this.activity.getContentResolver(), bitmap, imgPath, "");
			*/

			// アンドロイドのデータベースへ登録(登録しないとギャラリーなどにすぐに反映されないため)
			this.registAndroidDb(imgPath);
			//this.registAndroidDb(Utils.getTodayImageFullPath());
			Log.i("Photo", "Picture was taken successfully");
			this.takeBeforeFragment.onButtonPressed(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 泥のデータベースに撮った写真を登録する(そうしないとギャラリーとかに即反映されない)
	 * @param path 画像のフルパス
	 */
	private void registAndroidDb(String path){
		ContentValues values = new ContentValues();
		ContentResolver contentResolver = activity.getContentResolver();
		values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
		values.put("_data", path);
		contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
	}
}
